docker-sqitch
=============

[![build status](https://gitlab.com/jmabey/docker-sqitch/badges/master/build.svg)](https://gitlab.com/jmabey/docker-sqitch/commits/master)

* [Sqitch homepage](http://sqitch.org/)

Tags
----

All images use Alpine Linux as a base.

* `latest`: Sqitch without any database drivers installed.
* `postgres`: Sqitch with PostgreSQL support.
* `postgres-sh`: Sqitch with PostgreSQL support and without an `ENTRYPOINT`. Useful for running as an image in GitLab CI, which requires a shell.

Example
-------

```
docker run --rm \
	--link running_postgres_container:postgres \
	--volume /path/to/sqitch/project:/src \
	--env PGHOST=postgres \
	--env PGUSER=postgres \
	--env PGPASSWORD=password \
	--env PGDATABASE=database \
	sqitch:postgres status
```
